#include <stdio.h>
#include <unistd.h>

int main(void){
	//reset the terminal
	printf("\033[2J");
	//terminal size
	int Tx = 200; //111
	int Ty = 60; //34
	long double xMin = -2.5;
	//long double xMin = -1.48209;
	long double xMax =  1.0;
	//long double xMax = -1.48208;
	long double yMin = -1.0;
	//long double yMin =  0.00042;
	long double yMax =  1.0;
	//long double yMax =  0.00045;

	//lets try to zoom
	//zoom vars
	float test = 0.01;
	int zoom_rounds = 1000;
	int zl = 1;
//	for(int zim = 0; zim < zoom_rounds; zim++){
//	while(1){
		/*char in = getchar();
			if( in == '+' ){
				xMin *= 0.5*10;
				xMax *= 0.5*10;
				yMin *= 0.5*10;
				yMax *= 0.5*10;
				zl++;
			} else if( in == '-' ){
				xMin *= 2.0/10;
				xMax *= 2.0/10;
				yMin *= 2.0/10;
				yMax *= 2.0/10;
				if( zl > 1 ){ zl--; }
			} else if( in == 'w'){
				yMin += 0.5/zl;
				yMax += 0.5/zl;
			} else if( in == 'a'){
				xMin -= 0.5/zl;
				xMax -= 0.5/zl;
			} else if( in == 's'){
				yMin -= 0.5/zl;
				yMax -= 0.5/zl;
			} else if( in == 'd'){
				xMin += 0.5/zl;
				xMax += 0.5/zl;
			} else if( in == 'q'){ 
				return 0;
			};*/
		
		//move the terminal cursor to top right
		printf("\33[0;0H");
		//render&calculator
		#pragma omp parallel for
		for(int Py=0; Py<Ty; Py++){//pixle X and Y coordinate
		for(int Px=0; Px<Tx; Px++){
			//pixel coordinate scaled to the mandel set "-2,5, 1"
			//float Sx = (float) ( Px - 0 ) / ( Tx - 0 );
			//float Sx = (float) (( Px * 3.5 ) / Tx ) -2.5;
			long double Sx = (double) (( Px * ( xMax - xMin ) ) / Tx ) + xMin;
			//float Sy = (float) ( Py - 0 ) / ( Ty - 0 );
			//float Sy = (float) Py*2 / Ty - 1.0;
			long double Sy = (double) (( Py *  (yMax - yMin ) ) / Ty ) + yMin;
			int m;
			long double ix=0, iy=0;
			int i;		//iterator
			int mi = 128;	//max iterations
			for (i = 0; i < mi && ix*ix+iy*iy <= 2*2; i++){
				long double t = ix*ix-iy*iy+Sx;
				iy = 2*ix*iy+Sy;
				ix = t;
			}

			if	(i < mi/16*1)	{ printf("\33[40m ");  }
			else if (i < mi/16*2)	{ printf("\33[100m "); }
			else if (i < mi/16*3)	{ printf("\33[45m ");  }
			else if (i < mi/16*4)	{ printf("\33[105m "); }
			else if (i < mi/16*5)	{ printf("\33[44m ");  }
			else if (i < mi/16*6)	{ printf("\33[104m "); }
			else if (i < mi/16*7)	{ printf("\33[46m ");  }
			else if (i < mi/16*8)	{ printf("\33[106m "); }
			else if (i < mi/16*9)	{ printf("\33[42m ");  }
			else if (i < mi/16*10)	{ printf("\33[102m "); }
			else if (i < mi/16*11)	{ printf("\33[43m ");  }
			else if (i < mi/16*12)	{ printf("\33[103m "); }
			else if (i < mi/16*13)	{ printf("\33[41m ");  }
			else if (i < mi/16*14)	{ printf("\33[101m "); }
			else if (i < mi/16*15)	{ printf("\33[47m ");  }
			else			{ printf("\33[107m "); }
		}
			printf("\n");
		}
		printf("\n");

//		glBegin();
//			glRasterPos2i(Px, Py);
//			glDrawPixels(1, 1, glUnsignedShort, &i)
//		glEnd();

//		xMin *= 0.7;
//		xMax *= test;
//		yMin *= 0.7;
//		yMax *= test;
//		test /= 8;
//		sleep(1);
		//printf("\033[2J");
//	}
}
