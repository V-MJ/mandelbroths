variable xn	 0E	xn  F!	: x	xn  F@ ;
variable xin	 0E	xin F!	: xi	xin F@ ;
: xmin		-25E-1	;	: xmax	1E ;
variable yn	 0E	yn  F!	: y	yn  F@ ;
variable yin	 0E	yin F!	: yi	yin F@ ;
: ymin		-1E	;	: ymax	1E ;
variable tn	 0E	tn  F!	: t	tn  F@ ;
variable ite 0E ite F! : item 61E ; : tx 213E ( 106 ) ; : ty 61E ( 28 ) ;
( important!! item max must = ty for proper funtionality)

: exzit
	x x F* y y F* F+ 4E F<
	ite F@ item F<= xor
;
( scales work )
: scalevarx xmax xmin F- tx F/ xi F* xmin F+ ;
: scalevary ymax ymin F- ty F/ yi F* ymin F+ ;
( should work? )
: newx x x F* y y F* F-	scalevarx F+ ;
: newy 2E x F* y F*	scalevary F+ ;

: point
	0E ite F!
	begin
	newx tn F! newy yn F!
	tn F@ xn F!
	exzit
	ite F@ 1E F+ ite F!
	until
;
: pallette
	ite F@ item F/ 25E F* F>S ( should work )
	dup 0 = if   32 emit then	dup 1 = if  '.' emit then
	dup 2 = if  ',' emit then	dup 3 = if  '-' emit then
	dup 4 = if  '+' emit then	dup 5 = if  '<' emit then
	dup 6 = if  '=' emit then	dup 7 = if  'u' emit then
	dup 8 = if  'c' emit then	dup 9 = if  'n' emit then
	dup 10 = if 'z' emit then	dup 11 = if 's' emit then
	dup 12 = if 'o' emit then	dup 13 = if 'e' emit then
	dup 14 = if 'm' emit then	dup 15 = if 'a' emit then
	dup 16 = if 'd' emit then	dup 17 = if 'O' emit then
	dup 18 = if 'Q' emit then	dup 19 = if 'G' emit then
	dup 20 = if 'G' emit then	dup 21 = if '9' emit then
	dup 22 = if '8' emit then	dup 23 = if '0' emit then
	dup 24 = if '%' emit then	dup 25 = if '$' emit then
	drop
;
: mandel ( x y -- )
	( actually do the mandel )
	ty F>S 0 do I S>F yin F!	( the y loop )
	tx F>S 0 do I S>F xin F!	( the x loop )
			( calculate a point )
		point pallette
			( reset variables )
		0E xn F! 0E yn F! 0E tn F! 0E ite F!
	loop
	CR
	loop
; mandel bye
