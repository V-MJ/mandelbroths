package main
import (//"fmt"
	"image"
	"image/color"
	"image/png"
	"log"
	"os"
)
//common variables
var Tx int = 7680;	//image size
var Ty int = 4320;
var xMin float64 = -2.5;//defines of a mandelbrot position
var xMax float64 =  1.0;
var yMin float64 = -1.0;
var yMax float64 =  1.0;

//consumes x and y cordinates and a pointer to a image.NRGBA
func mkpix(Px int, Py int, img image.NRGBA){
	//pixel coordinate scaled to the mandel set "-2,5, 1"
	var Sx = (( float64(Px) * ( xMax - xMin ) ) / float64(Tx) ) + xMin;
	var Sy = (( float64(Py) * ( yMax - yMin ) ) / float64(Ty) ) + yMin;
	var ix float64;
	var iy float64;
	var i int;	//iterator
	var mi int = 1024;//max iterations
	for i = 0; (i < mi) && (ix*ix+iy*iy <= 2*2); i++ {
		var t float64 = ix*ix-iy*iy+Sx;
		iy = 2*ix*iy+Sy;
		ix = t;
	}
	//select the color of a pixel
	img.Set(Px, Py, color.NRGBA{
		R: uint8(mi/i*13*255),
		G: uint8(mi/i*17*255),
		B: uint8(mi/i*21*255),
		A: 255,
	})
}

func main(){
	//render to png
	img := image.NewNRGBA(image.Rect(0, 0, Tx, Ty))
		//render&calculator
		for Py := 0; Py < int(Ty); Py++ {//pixle X and Y coordinate
		for Px := 0; Px < int(Tx); Px++ {
			go mkpix(Px, Py, *img);

		}}

		f, err := os.Create("image.png")
		if err != nil {
			log.Fatal(err)
		}

		if err := png.Encode(f, img); err != nil {
			f.Close()
			log.Fatal(err)
		}

		if err := f.Close(); err != nil {
			log.Fatal(err)
		}

}
